var MAP, INFOWINDOW, PLAYER_MARKER, DELIVERY_MARKER;
var playerXP = 0;
var playerLevel = 1;
var defaultDeliveryXP = 10;
var weatherXPMultiplier = 1;
var radiusXPMultiplier = 1;
var circleSelector;
var geoLocationWatcher;
var maxPlayRadius = 1000;
var currentDeliveryTime = 0;
var timer;
var playerInitialLocation;
var deliveryOngoing;
var currentWeatherType;
var currentDeliveryDistance;

// enum for delivery status
const deliveryStatus = Object.freeze({ "NOT_IN_PROGRESS": 0, "INITIAL_PICKUP": 1, "PARCEL_PICKED_UP": 2, "DELIVERY_COMPLETE": 3 });
var currentDeliveryStatus = deliveryStatus.NOT_IN_PROGRESS;

var geoLocationWatcherOptions = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

function initMap() {
    console.log("init");
    var place = { lat: 42.601, lng: 19.763 };
    var iconPicture = 'player.png';

    // The map, centered at lat/lon
    MAP = new google.maps.Map(
        document.getElementById('map'), {
        zoom: 16,
        center: place,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: true,
        fullscreenControl: false
    });
    INFOWINDOW = new google.maps.InfoWindow;
    // The marker, positioned at lat/lon

    PLAYER_MARKER = new google.maps.Marker({
        position: place,
        map: MAP,
        icon: {
            anchor: new google.maps.Point(16, 16),
            size: new google.maps.Size(32, 32),
            url: iconPicture
        }
    });

    getLocation();

    geoLocationWatcher = navigator.geolocation.watchPosition(geoLocationWatcherSuccess, geoLocationWatcherError, geoLocationWatcherOptions);

}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
    else {
        alert("Geolocation is not supported by this browser. Delivery Game cannot function.");
    }
}

var initialLocation = true;
function showPosition(position) {
    var pos = { lat: parseFloat(position.coords.latitude), lng: parseFloat(position.coords.longitude) };

	var currentZoom = MAP.getZoom();
    MAP.setCenter(pos);
    PLAYER_MARKER.setPosition(pos);
	MAP.setZoom(currentZoom);

    // display buttons when location is found
    if (initialLocation) {
        document.getElementById('startButton').hidden = false;
        document.getElementById('msg').innerHTML = '';
        initialLocation = false;
    }

    if (deliveryOngoing) {
        drawLineBetweenPlayerAndParcel();

        if (getPlayerDeliveryDistance() < 10) {
            if (currentDeliveryStatus === deliveryStatus.INITIAL_PICKUP) {
                currentDeliveryStatus = deliveryStatus.PARCEL_PICKED_UP;
                alert("Delivery has been picked up! Return to the initial location!");
                beginReturnTrip();
            }
            else if (currentDeliveryStatus === deliveryStatus.PARCEL_PICKED_UP) {
                currentDeliveryStatus = deliveryStatus.DELIVERY_COMPLETE;

                //add distance XP
                let distanceXP = (2*currentDeliveryDistance)/(currentDeliveryTime/60);
                updateXP(distanceXP); 

                endDelivery();
                alert("You delivered the parcel! You're winner!");
            }

        }
    }

}

// draw line between player marker and parcel marker to make finding the parcel easier whilst zoomed in
var LINE;
function drawLineBetweenPlayerAndParcel() {
    if (PLAYER_MARKER && DELIVERY_MARKER) {
        if (LINE) LINE.setMap(null); //clear previous line

        LINE = new google.maps.Polyline({
            path: [PLAYER_MARKER.getPosition(), DELIVERY_MARKER.getPosition()],
            geodesic: true,
            strokeColor: "#000000",
            strokeOpacity: 0,
            icons: [{
                icon: {
                    path: 'M 0, -1 0,1',
                    strokeOpacity: 0.75,
                    scale : 4
                },
                offset: '0',
                repeat: '20px'
            }],
            strokeWeight: 2
        });
    
        LINE.setMap(MAP);
    }
}

function geoLocationWatcherSuccess(pos) {
    showPosition(pos);
}

function geoLocationWatcherError(err) {
    console.warn("geolocation watcher error " + err.message);
}

// called after the user presses the start button
// allows the user the choose a play area
function chooseArea() {
    if (!circleSelector) { // check ensures that multiple circle selectors cannot exist simultaneously
        // hide START button and reveal FIND NEW DELIVERY button
        document.getElementById('startButton').hidden = true;
        document.getElementById('spawnButton').hidden = false;

        // center the map on the player marker
        MAP.setCenter(PLAYER_MARKER.getPosition());

        // display helpful information
        INFOWINDOW.close(); // close any previous info windows
        INFOWINDOW.setPosition(MAP.center);
        INFOWINDOW.setContent("Choose your play area using the circle selector and select FIND NEW DELIVERY. The larger the area, the more XP you can get! As you level up, the maximum area (and XP multiplier) increase.");
        INFOWINDOW.open(MAP);

        // add an editable and draggable circle centered on the player marker
        circleSelector = new google.maps.Circle({
            strokeColor: '#000000',
            strokeOpacity: 0.8,
            fillColor: '#FF0000',
            fillOpacity: 0.4,
            map: MAP,
            center: PLAYER_MARKER.getPosition(),
            radius: 350,
            editable: true,
            draggable: true
        });
        radiusXPMultiplier = 1;

        // change the circle's radius to the minimum (200) if it becomes too small, or the maximum (varies based on player level) if it becomes too big
        // changes the color of the circle depending on the radius to indicate the XP multiplier
        // also ensures that the player is in the chosen area by re-centering if necessary
        google.maps.event.addListener(circleSelector, 'radius_changed', function () {
            var radius = circleSelector.getRadius();
            // console.log(radius);

            // ensure that the size is within set bounds
            if (radius < 200) {
                circleSelector.setOptions({
                    radius: 200
                });
            } else if (radius > maxPlayRadius) {
                circleSelector.setOptions({
                    radius: maxPlayRadius
                });
            }

            // change color depending on radius
            if (radius <= 500) {
                // red
                circleSelector.setOptions({
                    fillColor: '#FF0000'
                });
                radiusXPMultiplier = 1;
            } else if (radius <= 1000) {
                // yellow
                circleSelector.setOptions({
                    fillColor: '#F59C00'
                });
                radiusXPMultiplier = 1.5;
            } else if (radius <= 1500) {
                // green
                circleSelector.setOptions({
                    fillColor: '#00FF00'
                });
                radiusXPMultiplier = 2;
            } else if (radius <= 2000 && maxPlayRadius >= 2000) {
                // Robin's Egg Blue
                circleSelector.setOptions({
                    fillColor: '#00CCCC'
                });
                radiusXPMultiplier = 2.5;
            }  else if (radius <= 2500 && maxPlayRadius >= 2500) {
                // blue
                circleSelector.setOptions({
                    fillColor: '##0000CC'
                });
                radiusXPMultiplier = 3;
            } else if (radius <= 3000 && maxPlayRadius >= 3000) {
                // purple
                circleSelector.setOptions({
                    fillColor: '#3300CC'
                });
                radiusXPMultiplier = 3.5;
            } else if (radius <= 3500 && maxPlayRadius >= 3500) {
                // pink
                circleSelector.setOptions({
                    fillColor: '#CC00CC'
                });
                radiusXPMultiplier = 4;
            } else if (radius <= 4000 && maxPlayRadius >= 4000) {
                // turtle green
                circleSelector.setOptions({
                    fillColor: '#324611'
                });
                radiusXPMultiplier = 4.5;
            } else if (radius <= 4500 && maxPlayRadius >= 4500) {
                // British Racing Green
                circleSelector.setOptions({
                    fillColor: '#063E24'
                });
                radiusXPMultiplier = 5;
            } else if (radius <= 5000 && maxPlayRadius >= 5000) {
                // Tacao
                circleSelector.setOptions({
                    fillColor: '#E9A96F'
                });
                radiusXPMultiplier = 5.5;
            } else if (radius <= 5500 && maxPlayRadius >= 5500) {
                // bronze
                circleSelector.setOptions({
                    fillColor: '#CD7F32'
                });
                radiusXPMultiplier = 6;
            } else if (radius < 6000 && maxPlayRadius >= 6000) {
                // silver
                circleSelector.setOptions({
                    fillColor: '#C0C0C0'
                });
                radiusXPMultiplier = 6.5;
            } else if (radius == 6000) {
                // GOLD
                circleSelector.setOptions({
                    fillColor: '#FFD700'
                });
                radiusXPMultiplier = 7;
            }

            // updates the UI so that changes in the XP multiplier are indicated
            updateUIInfo();

            // ensure that the player marker is inside the chosen circular area; if not, re-center on the player marker
            var playerWithinCircle = google.maps.geometry.spherical.computeDistanceBetween(PLAYER_MARKER.getPosition(), circleSelector.getCenter()) <= circleSelector.getRadius();
            if (!playerWithinCircle) {
                circleSelector.setOptions({
                    center: PLAYER_MARKER.position
                });
            }
        });

        // called after the circle is dragged
        // ensure that the player marker is inside the chosen circular area; if not, re-center on the player marker
        google.maps.event.addListener(circleSelector, 'dragend', function () {
            var playerWithinCircle = google.maps.geometry.spherical.computeDistanceBetween(PLAYER_MARKER.getPosition(), circleSelector.getCenter()) <= circleSelector.getRadius();
            if (!playerWithinCircle) {
                circleSelector.setOptions({
                    center: PLAYER_MARKER.position
                });
            }
        });

        //Update weather
        getWeather();
    }
}

// updates the UI based on playerLevel, playerXP, weatherXPMultiplier and radiusXPMultiplier
function updateUIInfo() {
    if (playerLevel === 10) {
        document.getElementById('levelAndXP').innerHTML = '<b>LEVEL: </b> 10 (MAX!) <b>XP: </b> ' + playerXP.toFixed(0) + ' (×' + (radiusXPMultiplier + weatherXPMultiplier) + ')';
    } else {
        document.getElementById('levelAndXP').innerHTML = '<b>LEVEL: </b> ' + playerLevel + ' <b>XP: </b> ' + playerXP.toFixed(0) + ' (×' + (weatherXPMultiplier + radiusXPMultiplier) + ')';
    }
}

// called after the user has chosen a play area and selected FIND NEW DELIVERY
// spawns a delivery marker somewhere in the play area and has the user confirm the delivery or spawns another one
function spawnDelivery() {
    // TODO improve spawn logic (spawn on roads only?), add instructions

    // make the circle selector uneditable and uncolored
    circleSelector.setOptions({
        editable: false,
        draggable: false,
        fillOpacity: 0
    });

    // reveal ACCEPT DELIVERY button if the current delivery is in the picking up phase
    if (currentDeliveryStatus !== deliveryStatus.PARCEL_PICKED_UP) document.getElementById('acceptButton').hidden = false;

    // get random location within the bounds of the selected area
    var latMax = circleSelector.getBounds().getNorthEast().lat();
    var latMin = circleSelector.getBounds().getSouthWest().lat();
    var lngMax = circleSelector.getBounds().getNorthEast().lng();
    var lngMin = circleSelector.getBounds().getSouthWest().lng();
    var randomLocation = getRandomDeliveryLocation(latMax, latMin, lngMax, lngMin, circleSelector);

    // make any earlier delivery marker invisible
    if (DELIVERY_MARKER) {
        DELIVERY_MARKER.setVisible(false);
    }

    // Delivery marker
    var deliveryMarkerPicture = 'parcel.png';
    DELIVERY_MARKER = new google.maps.Marker({
        position: randomLocation,
        map: MAP,
        visible: true,
        icon: {
            anchor: new google.maps.Point(16, 16),
            size: new google.maps.Size(32, 32),
            url: deliveryMarkerPicture
        }
    });

    // TODO remove logging
    //console.log("Delivery location:")
    //console.log(randomLocation.toString());
	currentDeliveryStatus = deliveryStatus.NOT_IN_PROGRESS;
}

// returns a random location that is at least 50 meters away from the player but within the circle
// the return value is a LatLng
function getRandomDeliveryLocation(latMax, latMin, lngMax, lngMin, circle) {
    // the randomized location is within a rectangle, so it may not be within the circle; it may also be too close to the player
    // for these reasons, multiple executions may be necessary
    while (true) {
        var deliveryLat = Math.random() * (latMax - latMin) + latMin;
        var deliveryLng = Math.random() * (lngMax - lngMin) + lngMin;
        var randomLocation = new google.maps.LatLng({ lat: deliveryLat, lng: deliveryLng });
        if (google.maps.geometry.spherical.computeDistanceBetween(randomLocation, circle.getCenter()) <= circle.getRadius()) { // the location is within the circle
            if (google.maps.geometry.spherical.computeDistanceBetween(randomLocation, PLAYER_MARKER.getPosition()) >= 50) {
                return randomLocation;
            }
        }
    }
}

// starts the timer and makes it visible
// starts the game
function beginDelivery() {
    
    // save the initial location for the return trip
    playerInitialLocation = PLAYER_MARKER.getPosition();

    // hide the FIND NEW DELIVERY and ACCEPT DELIVERY buttons
    document.getElementById('spawnButton').hidden = true;
    document.getElementById('acceptButton').hidden = true;

    // start timer and make elapsed time visible in the UI

    // First start
    if (currentDeliveryStatus === deliveryStatus.NOT_IN_PROGRESS) {
        drawLineBetweenPlayerAndParcel();
    }

	currentDeliveryTime = 0; // elapsed time in ms
	currentDeliveryDistance = getPlayerDeliveryDistance();
	clearInterval(timer); // clear any earlier timers
	timer = setInterval(function () {
		currentDeliveryTime += 1000;

		// calculate current time in the format HH:MM:SS
		var hours = Math.floor(currentDeliveryTime / 1000 / 60 / 60);
		var minutes = Math.floor(currentDeliveryTime / 1000 / 60 % 60);
		var seconds = Math.round((currentDeliveryTime / 1000) % 60);

		// add padding zeroes
		if (hours < 10) hours = '0' + hours;
		if (minutes < 10) minutes = '0' + minutes;
		if (seconds < 10) seconds = '0' + seconds;

		// update UI
		document.getElementById("timer").innerHTML = '<b>TIME: </b>' + hours + ':' + minutes + ':' + seconds;
	}, 1000);


    deliveryOngoing = true;
    currentDeliveryStatus = deliveryStatus.INITIAL_PICKUP;
}

// begins the second half of the delivery, the return to the initial location
function beginReturnTrip() {
    if (DELIVERY_MARKER) {
        DELIVERY_MARKER.setVisible(false);
    }

    var deliveryMarkerPicture = 'parcel.png';
    DELIVERY_MARKER = new google.maps.Marker({
        position: playerInitialLocation,
        map: MAP,
        visible: true,
        icon: deliveryMarkerPicture
    });

    // TODO remove logging
    //console.log("Initial location:");
    //console.log(playerInitialLocation.toString());
}

// end the delivery
function endDelivery() {
    document.getElementById('spawnButton').hidden = false;
    document.getElementById('acceptButton').hidden = true;

    clearInterval(timer);

    updateXP(defaultDeliveryXP);

    circleSelector.setOptions({
        editable: true,
        draggable: true,
        fillColor: '#FF0000',
        fillOpacity: 0.4,
        radius: 350,
        center: PLAYER_MARKER.position
    });
    radiusXPMultiplier = 1;

    DELIVERY_MARKER.setVisible(false);

    getWeather();
	
	currentDeliveryStatus = deliveryStatus.NOT_IN_PROGRESS;
    deliveryOngoing = false;
}

// display an info window with information about the game
function showHelp() {
    INFOWINDOW.close(); // close any previous info windows
    INFOWINDOW.setPosition(MAP.center);
    INFOWINDOW.setContent("Welcome to DELIVERY GAME! In this game, you must retrieve a delivery and then return to your initial location. To play, press START and choose your play area." +
        "The larger the area, the more XP you can get! If it's raining or snowing you will get bonus XP! Level-ups allow you to select larger play areas.");
    INFOWINDOW.open(MAP);
}

// weather
function getWeather() {
    var coords = PLAYER_MARKER.getPosition();
    var weatherDiv = document.getElementById("weather");

    $.getJSON(`http://api.openweathermap.org/data/2.5/weather?lat=${coords.lat()}&lon=${coords.lng()}&units=metric&appid=739bb214f55beee866e28c32643eb1ad&lang=en`, function (data) {
        //console.log(data);

        weatherDiv.innerHTML = '<b>Location: </b>' + data.name + '  <b>Weather condition: </b>' + data.weather[0].main;

        let weatherParams = processWeatherType(data.weather[0].id);
        weatherXPMultiplier = weatherParams.xp;
        currentWeatherType = weatherParams.weatherType;
        if (MAP) MAP.setOptions({styles : weatherParams.style});

        updateUIInfo();

        //console.log(currentWeatherType);
        //console.log(weatherXPMultiplier);
    });
}

// returns the distance in meters between the player marker and the delivery marker
function getPlayerDeliveryDistance() {
    return google.maps.geometry.spherical.computeDistanceBetween(PLAYER_MARKER.getPosition(), DELIVERY_MARKER.getPosition());
}

// convert OpenWeatherMap weather id to something sensible
function processWeatherType(id) {
    if (id === 800) return { weatherType: "sunny", xp: 1 , style: STYLE_RETRO};

    switch (Math.floor(id / 100)) {
        case 2:
            return { weatherType: "thunder", xp: 3, style: STYLE_DARK };
        case 3:
            return { weatherType: "drizzle", xp: 1.5 , style: STYLE_DARK};
        case 5:
            return { weatherType: "rainy", xp: 2 , style: STYLE_LIGHT};
        case 6:
            return { weatherType: "snowy", xp: 2.5 , style: STYLE_SILVER};
        case 7:
            return { weatherType: "foggy", xp: 1.25 , style: STYLE_LIGHT};
        case 8:
            return { weatherType: "cloudy", xp: 1.1 , style: STYLE_LIGHT};
        default:
            return { weatherType: "unknown", xp: 1 , style: STYLE_LIGHT};
    }
}

// update player XP
function updateXP(amount, type = "+") {
    if (type === "+") {
        // TODO remove logging
        //console.log("Adding XP. Weather multiplier: " + weatherXPMultiplier + " Radius multiplier: " + radiusXPMultiplier);
        playerXP += (weatherXPMultiplier + radiusXPMultiplier) * amount;
    }
    else if (type === "-") {
        playerXP -= amount;
        if (playerXP < 0) playerXP = 0;
    }

    // level up logic: the maximum level is 10, each level up increases maxPlayRadius by 500 (the maximum is 6000), each level requires increasing amounts of XP
    var level = Math.floor(playerXP / 100);
    if (playerXP < 10) {
        level = 1;
    } else if (playerXP < 25) {
        level = 2;
    } else if (playerXP < 45) {
        level = 3;
    } else if (playerXP < 70) {
        level = 4;
    } else if (playerXP < 100) {
        level = 5;
    } else if (playerXP < 135) {
        level = 6;
    } else if (playerXP < 175) {
        level = 7;
    } else if (playerXP < 220) {
        level = 8;
    } else if (playerXP < 270) {
        level = 9;
    } else if (playerXP >= 270) {
        level = 10;
    }

    maxPlayRadius = 1000 + level * 500;
    playerLevel = level;
    updateUIInfo();
}

// TODO level up screen/popup?


// different style arrays
var STYLE_DARK =
[
    {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
    {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
    {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
    {
      featureType: 'administrative.locality',
      elementType: 'labels.text.fill',
      stylers: [{color: '#d59563'}]
    },
    {
      featureType: 'poi',
      elementType: 'labels.text.fill',
      stylers: [{color: '#d59563'}]
    },
    {
      featureType: 'poi.park',
      elementType: 'geometry',
      stylers: [{color: '#263c3f'}]
    },
    {
      featureType: 'poi.park',
      elementType: 'labels.text.fill',
      stylers: [{color: '#6b9a76'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{color: '#38414e'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry.stroke',
      stylers: [{color: '#212a37'}]
    },
    {
      featureType: 'road',
      elementType: 'labels.text.fill',
      stylers: [{color: '#9ca5b3'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry',
      stylers: [{color: '#746855'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry.stroke',
      stylers: [{color: '#1f2835'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'labels.text.fill',
      stylers: [{color: '#f3d19c'}]
    },
    {
      featureType: 'transit',
      elementType: 'geometry',
      stylers: [{color: '#2f3948'}]
    },
    {
      featureType: 'transit.station',
      elementType: 'labels.text.fill',
      stylers: [{color: '#d59563'}]
    },
    {
      featureType: 'water',
      elementType: 'geometry',
      stylers: [{color: '#17263c'}]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.fill',
      stylers: [{color: '#515c6d'}]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.stroke',
      stylers: [{color: '#17263c'}]
    }
  ];

var STYLE_LIGHT = JSON.parse(`[
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.business",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
  ]`);

var STYLE_SILVER = JSON.parse(`[
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f5f5f5"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#f5f5f5"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#bdbdbd"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#eeeeee"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "poi.business",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e5e5e5"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ffffff"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dadada"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e5e5e5"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#eeeeee"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#c9c9c9"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    }
  ]`);

  var STYLE_RETRO = JSON.parse(`[
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#523735"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#f5f1e6"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#c9b2a6"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#dcd2be"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#ae9e90"
        }
      ]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#93817c"
        }
      ]
    },
    {
      "featureType": "poi.business",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#a5b076"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#447530"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f5f1e6"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#fdfcf8"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f8c967"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#e9bc62"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e98d58"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#db8555"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#806b63"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8f7d77"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#b9d3c2"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#92998d"
        }
      ]
    }
  ]`);